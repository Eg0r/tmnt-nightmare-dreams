uses GraphABC,ABCSprites;

var s: spriteABC; w,h:integer;

begin

SetWindowCaption('TMNT Nighmare Dreams');
NormalizeWindow;

w:=windowwidth; 
h:=windowheight;

s:=SpriteABC.Create(round(w/4.2),round(h/1.454),'1.1.png');
  
  s.Add('2.1.png');
  s.Add('2.2.png');
  s.Add('2.3.png');
  s.Add('2.4.png');
  s.Add('2.5.png');
  
  s.Add('3.1.png');
  s.Add('3.2.png');
  s.Add('3.3.png');
  s.Add('3.4.png');
  
  s.Add('3.5.png');
  s.Add('3.6.png');
  s.Add('3.7.png');
  s.Add('3.8.png');
  
  s.Add('4.5.png');
  s.Add('4.6.png');
  s.Add('4.7.png');
  s.Add('4.8.png');
  
  s.Add('1.2.png');
  s.Add('1.3.png');
  
  s.Add('5.1.png');
  s.Add('5.2.png');
  
  s.Add('5.3.png');
  s.Add('5.4.png');
  s.Add('5.5.png');
  s.Add('5.6.png');
  
  s.Add('1.4.png');
  s.Add('1.5.png');
  s.Add('1.6.png');
  s.Add('1.7.png');
  
  s.Add('8.1.png');
  s.Add('8.2.png');
  s.Add('8.3.png');
  s.Add('8.4.png');
  
  s.Add('8.5.png');
  s.Add('8.6.png');
  s.Add('8.7.png');
  s.Add('8.8.png');
  
  s.Add('2.7.png');
  s.Add('2.8.png');
  
  
  s.Add('1.1m.png');
  s.Add('2.1m.png');
  s.Add('2.2m.png');
  s.Add('2.3m.png'); 
  s.Add('2.4m.png'); 
  s.Add('2.5m.png'); 
  
  s.Add('3.1m.png');
  s.Add('3.2m.png');
  s.Add('3.3m.png');
  s.Add('3.4m.png'); 
  
  s.Add('3.5m.png');
  s.Add('3.6m.png');
  s.Add('3.7m.png');
  s.Add('3.8m.png');
  
  s.Add('4.5m.png'); 
  s.Add('4.6m.png');
  s.Add('4.7m.png');
  s.Add('4.8m.png');
  
  s.Add('1.2m.png');
  s.Add('1.3m.png');
  
  s.Add('5.1m.png');
  s.Add('5.2m.png');
  
  s.Add('5.3m.png');
  s.Add('5.4m.png');
  s.Add('5.5m.png');
  s.Add('5.6m.png');
  
  s.Add('1.4m.png');
  s.Add('1.5m.png');
  s.Add('1.6m.png');
  s.Add('1.7m.png');
  
  s.Add('8.1m.png');
  s.Add('8.2m.png');
  s.Add('8.3m.png');
  s.Add('8.4m.png');
  
  s.Add('8.5m.png');
  s.Add('8.6m.png');
  s.Add('8.7m.png');
  s.Add('8.8m.png');
  
  s.Add('2.7m.png');
  s.Add('2.8m.png');
  
  s.AddState('Stand',1);
  s.AddState('Going',5);
  s.AddState('Strikeone',4);
  s.AddState('Striketwo',4);
  s.AddState('Strikethree',4);
  s.AddState('Down',2);
  s.AddState('Jumping',2);
  s.AddState('Jumpingable',4);
  s.AddState('DownStrike',4);
  s.AddState('GameOverone',4);
  s.AddState('GameOvertwo',4);
  s.AddState('StrikeOver',2);
  
  s.AddState('StandMirror',1);
  s.AddState('GoingMirror',5);
  s.AddState('StrikeoneMirror',4);
  s.AddState('StriketwoMirror',4);
  s.AddState('StrikethreeMirror',4);
  s.AddState('DownMirror',2);
  s.AddState('JumpingMirror',2);
  s.AddState('JumpingableMirror',4);
  s.AddState('DownStrikeMirror',4);
  s.AddState('GameOveroneMirror',4);
  s.AddState('GameOvertwoMirror',4);
  s.AddState('StrikeOverMirror',2);
  
  s.Transparent:=True;
  
  s.Speed:=9;
  s.SaveWithInfo('Raph.png');
end.