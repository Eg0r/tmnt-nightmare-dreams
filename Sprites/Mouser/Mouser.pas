uses GraphABC,ABCSprites;

var v: spriteABC; w,h:integer;

begin

SetWindowCaption('TMNT Nighmare Dreams');
NormalizeWindow;

w:=windowwidth; 
h:=windowheight;

v:=SpriteABC.Create(round(w/4.2),round(h/1.454),'1.1.png');
  
  v.Add('1.2.png');
  v.Add('1.3.png');
  v.Add('1.4.png');
  
  v.Add('2.1.png');
  v.Add('2.2.png');
  v.Add('2.3.png');
  v.Add('2.4.png');
  v.Add('2.5.png');
  v.Add('2.6.png');
 
  v.Add('3.1.png');
  v.Add('3.2.png');
  v.Add('3.3.png');
  v.Add('3.4.png');
  v.Add('3.5.png');
  v.Add('3.6.png');
  
  v.Add('1.5.png');
  v.Add('1.6.png');
  
  
  v.Add('1.1m.png');
  v.Add('1.2m.png');
  v.Add('1.3m.png');
  v.Add('1.4m.png');
  
  v.Add('2.1m.png');
  v.Add('2.2m.png');
  v.Add('2.3m.png');
  v.Add('2.4m.png');
  v.Add('2.5m.png');
  v.Add('2.6m.png');
 
  v.Add('3.1m.png');
  v.Add('3.2m.png');
  v.Add('3.3m.png');
  v.Add('3.4m.png');
  v.Add('3.5m.png');
  v.Add('3.6m.png');
  
  v.Add('1.5m.png');
  v.Add('1.6m.png');
  
  v.AddState('Stand',4);
  v.AddState('Going',6);
  v.AddState('GameOver',6);
  v.AddState('StrikeOver',2);
  
  v.AddState('StandMirror',4);
  v.AddState('GoingMirror',6);
  v.AddState('GameOverMirror',6);
  v.AddState('StrikeOverMirror',2);
  
  v.Transparent:=True;
  
  v.Speed:=9;
  v.SaveWithInfo('Mouser.png');
  
end.