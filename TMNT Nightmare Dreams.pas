{$mainresource icongame.res}
{$platformtarget x86}
{$reference 'Bass.net.dll'}
{$reference system.drawing.dll}
{$reference system.windows.forms.dll}
{$apptype windows}

Uses 

   System.Windows.Forms,
   System.Drawing,
   un4seen.Bass;
   
function GetKeyState(nVirtKey: integer): smallint; external 'User32.dll'; //�������� ������� ����������� ������� ������ �� Win Api    
   
function BASS_Init(device: integer; freq: integer; flags: Un4seen.Bass.BASSInit; win: System.IntPtr): boolean;
begin
 BASS_Init:=un4seen.Bass.bass.BASS_Init(device,freq,flags,win);
end;

function BASS_StreamCreateFile(lfile: string; offset: int64; length: int64; flags: Un4seen.Bass.BASSFlag): integer;
begin
 BASS_StreamCreateFile:=un4seen.Bass.Bass.BASS_StreamCreateFile(lfile,offset,length,flags);
end;  

//��������� ���� ��� ����������� ������� ������ � ����������� (��� �������� �� ������� �������� � integer 
Const

    MASK: word = $8000;
    VK_Left = 37;  
    VK_Right = 39;
    VK_Down = 40;
    VK_S = 83; 
    h = round(480 / 1.454); //������ �������� ������
   
Var 

   GameWindow := new Form;   
   sysinptr := System.IntPtr.Zero; 
   thg: System.Threading.Thread; thgs: System.Threading.ParameterizedThreadStart; 
   
Type   

   StatesSet = set of string;
   
   Sound = class
   private
     zv: integer;
   public
     ///loop ���������� ������������� �� ���� ���������� (����������� ���������� ���)
     constructor Create(fname: string; loop: boolean);
     procedure Play();
     function IsPlaying(): boolean;
     procedure SetVolume(Vol: real);
     procedure Stop();
     destructor Destroy();
   end;
   
   ButtonGame = class (button)
     ///���������� b ��������� ����� �� ���������� ������� ������� �����
     constructor Create(textb: string; w,h: integer; x,y: integer; cl: color; b: Control);
   end;
   
   TextGame = class (System.Windows.Forms.Label)
     ///���������� b ��������� ����� �� ���������� ������� ������� �����
     constructor Create(textt: string; size: integer; cl: Color; x,y: integer; b: Control);
   end;  
   
   Menu = class
   btexit, btstart, btoptions, btgamecreate, btexittwo, btcontine: ButtonGame;
   PausePanel: Panel; SoundBar: TrackBar;
   tgamecreate: array [1..3] of TextGame; //������ ������� ��� ���� "� ����"
   toptions: array [1..4] of TextGame; //������ ������� ��� ���� "�����"
   tstart: TextGame; //����� ��� ���� "������ ����"
   turtlebut: array [1..4] of ButtonGame; //������ ������ ��� ������ ���������
   SoundClick, Music: Sound;
     constructor Create();
     procedure PlayClick(sender: object; e: System.EventArgs);
     procedure GameStart;
     procedure GameCreate;
     procedure Options;
     procedure PreGame;
   end;
   
   GControl = class (Control)
   public
   IsGravi: boolean:=False;
   bm: BitMap;
   end;
   
   Indicator = class (GControl)
   private
   b: BitMap;
   g: Graphics;
   Live: integer:=0;
   F: integer:=0;
   mF: integer:=0;
   mLive: integer:=0;
   v: integer:=0;
   cl1,cl2: Color; 
   public
     ///x � y ������� �� ����, LeftORight ����� ���� �� �������� ����� ������: False - �����, True - ������
     constructor Create(x, y: integer; LeftOrRight: boolean);
     procedure Addp(fname: string);
     procedure AddindHP(w,h: integer; cl: Color);
     procedure AddindF(w: integer; cl: Color);
     procedure Repaint;
   end;
   
   SpriteState = record
    Name: string;   // ����� ���������
    Beg: integer;   // ������ ��������, ���������� �������� ���������
    Count: integer; // ����� ���������
  end;
   
   Sprite = class (GControl)
   private
   Width, Height: integer;
   Load: boolean:=False;
   One: boolean;
   number, tik, active: integer;
   an: boolean:=True; 
   ATimer: System.Timers.Timer;
   States: Array of SpriteState;
   Sprites: Array of BitMap;
   SpriteCount: integer:=0; //���������� ������ � �������
   nextstate: string;
   mirror: string; 
   public
     constructor Create(x,y: integer);
     procedure Sprite.SetStateName(name: string);
     procedure SetStateNameOne(name: string; next: string);
     function Sprite.GetStateName: string;
     procedure Animation(sender:object; e:System.Timers.ElapsedEventArgs);
     procedure LoadFromInf(inf: string);
     procedure Sprite.Start;
     procedure Sprite.Stop;
     procedure MoveOn(x,y: integer);
     function GetFrame: integer;
     property Frame: integer read GetFrame;
     function FrameCount: integer;
     property StateName: string read GetStateName write SetStateName;
     procedure MirrorCheck;
   end;
   
   Loading = class (Sprite)
   bf, bg: BitMap;
     constructor Create();
     procedure Play;
     function IsPlaying(): boolean;
     procedure Stop;
   end;
   
   //����� ������ ���� ������� ��� ����������
   Character = class (Sprite)
   livechecks: boolean:=False;
   distant: boolean:=False; //����� �� ���� ��
   Lives: integer; //���������� �������� ����� ���������
   des: boolean:= False;
   
   StrikeSound: Sound;
   
   TimerII: System.Timers.Timer;
   
   public event LocationChanged: procedure(s: object; e: System.EventArgs);
   private function GetLocation(): integer; 
   begin
      result := self.Left;
   end;
   private procedure SetLocation(m: integer); 
   begin
      LocationChanged(self, new System.EventArgs());
      self.Left := m;
   end;
   public property Locationing: integer read GetLocation write SetLocation; 
   private t: System.Timers.Timer;
   private procedure HelpT(sender: object; e: System.Timers.ElapsedEventArgs);
   public procedure Graviting;
   end;
   
   Turtle = class (Character)
   private
   ind: Indicator;
   tj: System.Timers.Timer; //������ ��� �������� ������
   IsJump: boolean; //������� �� ��������
   k: integer; //���������� ������� ���������
   public
     constructor Create(x,y: integer);
     ///��������� ��� ����� ���������, s - ��� ���������
     procedure LoadSound(s: string);
     ///������ ��������� � ������ ������
     procedure Teleport;
     procedure Stand;
     ///�������� ������
     procedure GoingR;
     ///�������� �����
     procedure GoingL;
     procedure AddLives(fname: string);
     ///������ ����
     procedure Strikeone;
     ///������ ����
     procedure Striketwo;
     ///������ ����
     procedure Strikethree;
     procedure DownStrike;
     ///��������������� ��������� ��� �������� ������
     procedure HelpJumping(sender: object; e: System.Timers.ElapsedEventArgs);
     ///������
     procedure Jumping;
     ///������ 
     procedure Down;
     procedure LiveCheck(l: integer);
     destructor Destroy;
   end;
   
   Foot = class (Character)
   TimerLive: System.Timers.Timer;
   public
     constructor Create(x,y: integer);
     procedure Going;
     procedure ForTimer(sender: object; e: System.Timers.ElapsedEventArgs);
     procedure ForTimerTwo(sender: object; e: System.Timers.ElapsedEventArgs);
     procedure Strike;
     ///��������� ��� ���������� ��
     procedure II(f: boolean);
     procedure LiveCheck(l: integer);
     ///�������� ��������� ������
     property SetLive: integer write LiveCheck;
     procedure MirrorCheck;
     destructor Destroy;
   end;
   
   FootY = class (Character)
   sur: GControl;
   interval: integer;
   TimerSur: System.Timers.Timer;
   Vector: integer:=0;
   public
     constructor Create(x,y: integer);
     procedure Strike;
     procedure LiveCheck(l: integer);
     procedure ForTimer(sender: object; e: System.Timers.ElapsedEventArgs);
     destructor Destroy;
   end;
   
   Mouser = class (Character)
   rast, proid: integer;
   f: boolean:=False;
   TimerLive: System.Timers.Timer;
   public
     ///x � y ���������� �������, a ��������� �� ������� ������������� ������
     constructor Create(x, y, a: integer);
     procedure ForTimer(sender: object; e: System.Timers.ElapsedEventArgs);
     procedure ForTimerTwo(sender: object; e: System.Timers.ElapsedEventArgs);
     procedure LiveCheck(l: integer);
     destructor Destroy;
   end;
   
   Levels = class (GControl)
   R, L: integer;
   lvlend: boolean:=False;
   distanend: integer;
   BackSound: Sound;
   public
     constructor Create(fname, fmname: string);
     procedure Play;
     procedure Stop;
   end;
   
   Secondmetr = class (GControl)
   time: string;
   tb: BitMap;
   milis, s, m: integer;
   t: System.Timers.Timer;
   g: System.Drawing.Graphics;
   public
     constructor Create();
     procedure Start;
     procedure ForTimer(sender: object; e: System.Timers.ElapsedEventArgs);
     procedure Clear;
   end;
   
   ///����������� ������� ��� ���������� ���� ������� �������
   GraphPanel = class (PictureBox)
   t, t2: System.Timers.Timer;
   g: Graphics;
   b: BitMap;
   k, k2: integer; //���������� ���������
   IsPausing: boolean:=False; //�� ����� �� ����������� ����
   public
   GSec: Secondmetr;
   GLvl: Levels; 
   GTurtle: Turtle;
   GFoot: Array of Character;
   GDop: Array of GControl;
     constructor Create();
     procedure SetSecondmetr;
     procedure Refreshing;
     procedure AddDop(a: GControl);
     procedure AddGL(a: Levels); 
     procedure AddGT(a: Turtle);
     procedure AddGF(a: Character);
     ///��������������� ��� �������
     procedure Pause;
     ///��������� ��� �������
     procedure Start;
     ///���������� ���� ��������
     procedure PaintOne(sender:object; e:System.Timers.ElapsedEventArgs);
     ///������������ ������� �������� �� ������
     procedure PaintTwo(sender: object; e: System.EventArgs);
     ///�������� �� ����������
     procedure PaintThree(sender: object; e: System.Timers.ElapsedEventArgs);
   end;
   
Var 

   {StrikeSet: StatesSet := ['Strikeone', 
                            'Striketwo', 
                            'Strikethree', 
                            'StrikeoneMirror', 
                            'StriketwoMirror', 
                            'StrikethreeMirror', 
                            'DownStrike',
                            'DownStrikeMirror',
                            'Strike',
                            'StrikeMirror'];}
   
   //������������ ��� ����������� ������������
   MirrorSet: StatesSet := ['StandMirror',
                            'GoingMirror',
                            'StrikeoneMirror',
                            'StriketwoMirror',
                            'StrikethreeMirror',
                            'DownMirror',
                            'JumpingMirror',
                            'JumpingableMirror',
                            'DownStrikeMirror'];
                                
   StandSet: StatesSet := ['Stand',
                           'StandMirror'];
                           
   DownSet: StatesSet := ['Down',
                          'DownMirror',
                          'DownStrike',
                          'DownStrikeMirror'];
                          
   GoingSet: StatesSet := ['Going',
                           'GoingMirror'];
                           
   JumpSet: StatesSet := ['Jumping',
                          'Jumpnigable',
                          'JumpingMirror',
                          'JumpnigableMirror'];
                          
                            
   Shell: Turtle;                         
   
   function Intersect(x, y: GControl): boolean;
   var r: boolean;
   begin
   R:=False;
   if y.Left>x.Left then
      begin
        if y.Left-x.Left - round(x.bm.Width/3) < 0 then r:=True;
      end;
   if x.Left>y.Left then
      begin
        if x.Left-y.Left - round(y.bm.Width/3) < 0 then r:=True;
      end;
   result:=r;
   end;
   
   {procedure Transpareting(b: System.Drawing.BitMap);
   var tmp: System.Drawing.Color;
      i, j: integer;
   begin
   tmp := System.Drawing.Color.Black;
   for i:=1 to b.Width do
      for j:=1 to b.Height do
      b.SetPixel(i, j, tmp);
   end;   }
   
   //����� 
   constructor Sound.Create(fname: string; loop: boolean);
   begin
   BASS_Init(-1,4410,un4seen.Bass.BASSInit.BASS_DEVICE_SPEAKERS,sysinptr); 
   if loop = True then
   zv := BASS_StreamCreateFile(fname,0,0,un4seen.Bass.BASSFlag.BASS_SAMPLE_LOOP)
   else
   zv := BASS_StreamCreateFile(fname,0,0,un4seen.Bass.BASSFlag.BASS_DEFAULT);
   SetVolume(0.4);
   end;
   
   procedure Sound.Play();
   begin
   un4seen.Bass.bass.BASS_ChannelPlay(zv,true);
   end;
   
   function Sound.IsPlaying(): boolean;
   begin
  
   end;
   
   procedure Sound.SetVolume(Vol: real);
   begin
   un4seen.Bass.Bass.BASS_ChannelSetAttribute(zv, un4seen.Bass.BASSAttribute.Bass_Attrib_Vol, vol);
   end;
   
   procedure Sound.Stop();
   begin
   un4seen.Bass.Bass.BASS_ChannelStop(zv);
   end;
   
   destructor Sound.Destroy();
   begin
   un4seen.Bass.Bass.BASS_ChannelStop(zv);
   un4seen.Bass.Bass.BASS_StreamFree(zv);
   end;
   
   //������ 
   constructor ButtonGame.Create(textb: string; w,h: integer; x,y: integer; cl: color; b: Control);
   begin
   b.Controls.Add(self);
   Text := textb;
   self.Font := new System.Drawing.Font(FontFamily.GenericSansSerif, 15, FontStyle.Regular, GraphicsUnit.Pixel);
   Width := w;
   Height := h;
   Left := x;
   Top := y-h;
   BackColor := cl;
   end;
   
   //����� 
   constructor TextGame.Create(textt: string; size: integer; cl: Color; x,y: integer;b: Control);
   begin
   b.Controls.Add(self);
   AutoSize := True;
   self.Text := textt;
   self.BackColor := Color.Transparent;
   self.Font := new System.Drawing.Font(FontFamily.GenericSansSerif, size, FontStyle.Regular, GraphicsUnit.Pixel);
   self.ForeColor := cl;
   self.Left := x;
   self.Top := y;
   end;
   
   //������� 
   constructor Menu.Create();
   var bm: BitMap;
   begin
   //�������� ��������� ����
   SoundClick := Sound.Create('Sounds/Voice/Button.wav',False);
   Music := Sound.Create('Sounds/Music/MenuTheme.mp3',True); Music.Play;
   
   btstart := ButtonGame.Create('������ ����',180,30,round(GameWindow.Width / 27), round(GameWindow.Height / 1.1707),Color.Green,GameWindow);
   btstart.Click += self.PlayClick;
   btoptions := ButtonGame.Create('�����',100, 30,round(GameWindow.Width / 2.998), round(GameWindow.Height / 1.1707),Color.YellowGreen,GameWindow);
   btoptions.Click += self.PlayClick;
   btgamecreate := ButtonGame.Create('� ����',140, 30,round(GameWindow.Width / 1.97), round(GameWindow.Height / 1.1707),Color.DarkSeaGreen,GameWindow);
   btgamecreate.Click += self.PlayClick;
   btexit := ButtonGame.Create('�����',130, 30,round(GameWindow.Width / 1.35), round(GameWindow.Height / 1.1707),Color.DarkGreen,GameWindow);
   btexit.Click += self.PlayClick;
   
   tgamecreate[1] := TextGame.Create('���� ����������� Alepahto Games',25, Color.Red,100,100,GameWindow);
   tgamecreate[2] := TextGame.Create('���� �������� � �������������� .NET ���������',25, Color.Red,10,200,GameWindow);
   tgamecreate[1].Visible := False;
   tgamecreate[2].Visible := False;

   toptions[1] := TextGame.Create('A, S, D - ������� ����',25, Color.Red,190,100,GameWindow);
   toptions[2] := TextGame.Create('Left, Right - ������',25, Color.Red,200,200,GameWindow);
   toptions[3] := TextGame.Create('Space, Down - ������� � ���������',25, Color.Red,100,300,GameWindow);
   toptions[1].Visible := False;
   toptions[2].Visible := False;
   toptions[3].Visible := False;

   tstart := TextGame.Create('�������� ���������', 45, Color.Red, 80,350,GameWindow);
   tstart.Visible := False;
   
   turtlebut[1] := ButtonGame.Create('',round(GameWindow.Width/4.0378),round(GameWindow.Height/2.5),0,round(GameWindow.Height/1.75), Color.Green,GameWindow);
   bm := Bitmap.Create('Pictures\Leo.png');
   bm := Bitmap.Create(bm,turtlebut[1].Width,turtlebut[1].Height);
   turtlebut[1].BackgroundImage := bm;
   turtlebut[2] := ButtonGame.Create('',round(GameWindow.Width/4.0378),round(GameWindow.Height/2.5),turtlebut[1].Width,round(GameWindow.Height/1.75), Color.Green,GameWindow);
   bm := Bitmap.Create('Pictures\Don.png');
   bm := Bitmap.Create(bm,turtlebut[2].Width,turtlebut[2].Height);
   turtlebut[2].BackgroundImage := bm;
   turtlebut[3] := ButtonGame.Create('',round(GameWindow.Width/4.0378),round(GameWindow.Height/2.5),turtlebut[1].Width+turtlebut[2].Width,round(GameWindow.Height/1.75), Color.Green,GameWindow);
   bm := Bitmap.Create('Pictures\Raph.png');
   bm := Bitmap.Create(bm,turtlebut[3].Width,turtlebut[3].Height);
   turtlebut[3].BackgroundImage := bm;
   turtlebut[4] := ButtonGame.Create('',round(GameWindow.Width/4.0378),round(GameWindow.Height/2.5),turtlebut[1].Width+turtlebut[2].Width+turtlebut[3].Width,round(GameWindow.Height/1.75), Color.Green,GameWindow);
   bm := Bitmap.Create('Pictures\Mike.png');
   bm := Bitmap.Create(bm,turtlebut[4].Width,turtlebut[4].Height);
   turtlebut[4].BackgroundImage := bm;
   for var i:=1 to 4 do
   begin
   turtlebut[i].Visible := False;
   turtlebut[i].Click += self.PlayClick;
   end;
   
   //�������� ���������������� ����
   PausePanel := Panel.Create();
   GameWindow.Controls.Add(PausePanel);
   PausePanel.Width:=256;
   PausePanel.Height:=264;
   PausePanel.Top:=100;
   PausePanel.Left:=round(GameWindow.Width/2)-round(PausePanel.Width/2);
   PausePanel.BackColor := Color.OliveDrab;
   btexittwo := ButtonGame.Create('�����',130, 30,60, 240,Color.DarkGreen,PausePanel);
   btcontine := ButtonGame.Create('��������� � ����',180,30,40, 50,Color.Green,PausePanel);
   SoundBar := TrackBar.Create();
   SoundBar.Top := 130;
   SoundBar.Width := 200;
   SoundBar.Left := round(PausePanel.Width/2)-round(SoundBar.Width/2);
   PausePanel.Controls.Add(SoundBar);
   toptions[4] := TextGame.Create('������� ����� � ����',19,Color.YellowGreen,25,90,PausePanel);
   PausePanel.Visible := False;
   SoundBar.Value := 4;
   end;
   
   procedure Menu.PlayClick(sender: object; e: System.EventArgs);
   begin
   SoundClick.Play;
   end;
   
   procedure Menu.GameStart;
   begin
   tgamecreate[1].Visible := False;
   tgamecreate[2].Visible := False;

   toptions[1].Visible := False;
   toptions[2].Visible := False;
   toptions[3].Visible := False;

   btstart.Visible := False;
   btoptions.Visible := False;
   btgamecreate.Visible := False;
   btexit.Visible := False;

   tstart.Visible := True;

   turtlebut[1].Visible := True;
   turtlebut[2].Visible := True;
   turtlebut[3].Visible := True;
   turtlebut[4].Visible := True;
   end;
   
   procedure Menu.GameCreate;
   begin
   tgamecreate[1].Visible := True;
   tgamecreate[2].Visible := True;

   toptions[1].Visible := False;
   toptions[2].Visible := False;
   toptions[3].Visible := False;
   end;
   
   procedure Menu.Options;
   begin
   tgamecreate[1].Visible := False;
   tgamecreate[2].Visible := False;

   toptions[1].Visible := True;
   toptions[2].Visible := True;
   toptions[3].Visible := True;
   end;
   
   procedure Menu.PreGame;
   begin
   turtlebut[1].Visible := False;
   turtlebut[2].Visible := False;
   turtlebut[3].Visible := False;
   turtlebut[4].Visible := False;
   tstart.Visible := False;
   Music.Stop;
   end;
   
   //����������
   constructor Indicator.Create(x, y: integer; LeftOrRight: boolean);
   begin
   self.Left := x;
   self.Top := y;
   end;
   
   procedure Indicator.Addp(fname: string);
   begin
   bm := BitMap.Create(110,60);
   g := Graphics.FromImage(bm);
   b := BitMap.Create(fname);
   g.DrawImage(b, new Rectangle(new Point(0, 0), new System.Drawing.Size(b.Width, b.Height)));
   end;
   
   procedure Indicator.AddindHP(w,h: integer; cl: Color);
   begin
   {p.Width:=w+4;
   p.Backgroundimage := new Bitmap(p.width,p.height);}
   g.FillRectangle(new SolidBrush(Color.Black),0,35,w+4,h+4);
   g.FillRectangle(new SolidBrush(cl),2,37,w,h);
   cl1:=cl;
   Live:=w;
   mLive:=w;
   v:=h;
   end;
   
   procedure Indicator.AddindF(w: integer; cl: Color);
   begin
   g.FillRectangle(new SolidBrush(Color.Black),0,35+v+2,w+4,v+4);
   g.FillRectangle(new SolidBrush(cl),2,35+v+4,w,v);
   cl2:=cl;
   F:=w;
   mF:=w;
   end;
   
   procedure Indicator.Repaint;
   begin
   g.Clear(Color.Transparent);
   g.DrawImage(b, new Rectangle(new Point(0, 0), new System.Drawing.Size(b.Width, b.Height)));
   g.FillRectangle(new SolidBrush(Color.Black),0,35,mLive+4,v+4);
   g.FillRectangle(new SolidBrush(cl1),2,37,Live,v);
   g.FillRectangle(new SolidBrush(Color.Black),0,35+v+2,mF+4,v+4);
   g.FillRectangle(new SolidBrush(cl2),2,35+v+4,F,v);
   end;
   
   //������� 
   constructor Sprite.Create(x,y: integer);
   begin
   IsGravi:=True;
   self.Left := x;
   self.Top := y;
   end;
   
   procedure Sprite.SetStateName(name: string);
   begin
   number := 1;
   for var i:=0 to States.Length-1 do
          if States[i].Name = name then active := i;
   {for var i:=0 to active do
          number := number + States[i].Count;
   number -= States[active].Count;   }
   tik := 0;
   One:=False;
   an:=True;
   end;
   
   procedure Sprite.SetStateNameOne(name: string; next: string);
   begin
   number := 1;
   for var i:=0 to States.Length-1 do
          if States[i].Name = name then active := i;
   tik := 0;
   One:=True;
   an:=True;
   nextstate:=next;
   end;
   
   function Sprite.GetStateName: string;
   begin
   Result := States[active].Name;
   end;
   
   procedure Sprite.Animation(sender:object; e:System.Timers.ElapsedEventArgs);
   begin   
   if an=True then
   Begin
   if One=False then
      begin
        bm := Sprites[States[active].Beg+tik-1];
        tik+=1;
        if tik=States[active].Count then tik:=0; 
      end
   else   
       begin
        bm := Sprites[States[active].Beg+tik-1];
        tik+=1;
        if tik=States[active].Count then begin tik:=0; an:=False; if nextstate<>'' then StateName := nextstate; end;
       end;
   End;    
   end;
   
   procedure Sprite.LoadFromInf(inf: string);
   var 
   vs,sname: string;
   f: PABCSystem.text;
   i,j,w,sp,num,h: integer;
   helpb,bmp: BitMap;
   begin
   if Load=False then
   begin
    assign(f,inf);
    reset(f);
    readln(f,vs);
    j := Pos(' ',vs);
    inf := inf.Remove(inf.IndexOf('.alepahto'),Length('.alepahto')) + '.png';
    readln(f,w);
    readln(f,h);
    self.Width:=w;
    self.Height:=h;
    readln(f,num);
    SetLength(States,num);
    for i:=0 to num-1 do
    begin
      readln(f,vs);
      j := Pos(' ',vs);
      sname := Copy(vs,1,j-1);
      Delete(vs,1,j);
      vs := TrimLeft(vs);
      if j>0 then
        vs := Copy(vs,1,j-1);
        States[i].Name := sname;
        States[i].Count := StrToInt(vs);
        SpriteCount += States[i].Count;
        States[i].Beg := SpriteCount - States[i].Count + 1;
    end;
    close(f);
    bmp := BitMap.Create(inf);
    bmp.MakeTransparent(bmp.GetPixel(0,0));
    SetLength(Sprites,SpriteCount);
    
    for i:=0 to SpriteCount-1 do
    begin
    var cloneRect := new RectangleF(i*w, 0, w, h );
    helpb := bmp.Clone(cloneRect, bmp.PixelFormat);
    Sprites[i] := helpb;
    end;
    StateName := 'Stand';
    ATimer := System.Timers.Timer.Create(70);
    ATimer.Elapsed+=Animation;
    ATimer.Start;
    Load:=True;
    bm := Bitmap.Create(Sprites[0]);
   end; 
   end;
   
   procedure Sprite.Start;
   begin
   an:=True;
   end;
   
   procedure Sprite.Stop;
   begin
   an:=False;
   end;
   
   procedure Sprite.MoveOn(x,y: integer);
   begin
   self.Left += x;
   self.Top += y;
   end;
   
   function Sprite.GetFrame: integer;
   begin
   Result := tik;
   end;
   
   function Sprite.FrameCount: integer;
   begin
   Result := States[active].Count;
   end;
   
   
   procedure Sprite.MirrorCheck;
   begin
   If StateName in MirrorSet
   then mirror:='Mirror' else mirror:='';
   end;
   
   //����� ��������
   constructor Loading.Create();
   begin
   end;
   
   procedure Loading.Play;
   begin
   end;
   
   function Loading.IsPlaying(): boolean;
   begin
   end;
   
   procedure Loading.Stop;
   begin
   end;
   
   procedure Character.HelpT(sender: object; e: System.Timers.ElapsedEventArgs);
   begin
   self.Top += 1;
   if self.Top = h then begin t.Enabled:=False; self.MirrorCheck; self.StateName := 'Stand' + self.mirror; end;
   end;
   
   procedure Character.Graviting;    
   begin
   if t = nil then 
     begin
     t := System.Timers.Timer.Create(15);
     t.Elapsed += self.HelpT;
     end;
   t.Enabled := True;
   end;
   
   //����� 
   procedure Turtle.Stand;
   begin
   Start;
   MirrorCheck;
   if IsJump=False then StateName:='Stand'+mirror;
   end;
   
   procedure Turtle.GoingR;
   var v: integer:=0;
   begin
   if (StateName <> 'Going') and (StateName <> 'Jumpingable') and (StateName <> 'JumpingableMirror') then StateName := 'Going'; 
   if (self.Left<GameWindow.Width+round(self.Width/4)-self.Width) 
   and not((self.Left>GameWindow.Width-GameWindow.Width/3) and (self.Parent.ClientSize.Height>0)) 
   then v := 4;
   self.Locationing += v;
   end;
   
   procedure Turtle.GoingL;
   var v: integer:=0;
   begin
   if (StateName <> 'GoingMirror') and (StateName <> 'Jumpingable') and (StateName <> 'JumpingableMirror') then StateName := 'GoingMirror';
   if (self.Left>-self.Width/4) 
   and not((self.Left<GameWindow.Width/3-self.Width) and (self.Parent.ClientSize.Width>0))
   then v := 4;
   self.Locationing -= v;
   end;
   
   constructor Turtle.Create(x,y: integer);
   begin
   IsJump:=False;
   tj := System.Timers.Timer.Create(50);
   tj.Elapsed += self.HelpJumping;
   IsGravi:=True;
   self.Left := x;
   self.Top := y;
   ind := Indicator.Create(10,13,False);
   end;
   
   procedure Turtle.LoadSound(s: string);
   begin
   if (s='Leo') or (s='Raph') then StrikeSound := Sound.Create('Sounds/Voice/StrikeSword.wav',False)
   else StrikeSound := Sound.Create('Sounds/Voice/StrikeBo.wav',False);
   end;
   
   procedure Turtle.AddLives(fname: string);
   begin
   ind.Addp(fname);
   ind.AddindHP(100,6,Color.Red);
   ind.AddindF(66,Color.MediumSpringGreen);
   end;
  
   procedure Turtle.Strikeone;
   begin
   if not (pos('Damaging',self.StateName)<>0) and (StateName <> 'Jumpingable') and (StateName <> 'JumpingableMirror') then
   begin
   StrikeSound.Play;
   MirrorCheck;
   if (StateName='Down'+mirror) or (StateName='DownStrike'+mirror) then DownStrike else
   {if (GetKeyState(VK_Down) and MASK)=0 then begin
   IF (GetKeyState(VK_Down) and MASK)=0 THEN BEGIN 
   if ((GetKeyState(VK_S) and MASK) > 0) then if ((GetKeyState(VK_S) and MASK) > 0) then 
   begin  StateName := 'Strikethree'+mirror; //ToFront; 
   end else else} SetStateNameOne('Strikeone'+mirror,'Stand'+mirror); //ToFront;
   end;
   end;
   
   procedure Turtle.Striketwo;
   begin
   if not (pos('Damaging',self.StateName)<>0) and (StateName <> 'Jumpingable') and (StateName <> 'JumpingableMirror') then
   begin
   StrikeSound.Play;
   MirrorCheck;
   SetStateNameOne('Striketwo'+mirror,'Stand'+mirror);
   end;
   end;
   
   procedure Turtle.Strikethree;
   begin
   if not (pos('Damaging',self.StateName)<>0) and (StateName <> 'Jumpingable') and (StateName <> 'JumpingableMirror') then
   begin
   StrikeSound.Play;
   MirrorCheck;
   SetStateNameOne('Strikethree'+mirror,'Stand'+mirror);
   end;
   end;
   
   procedure Turtle.Down;
   begin
   MirrorCheck;
   if (StateName <> 'Down' + mirror) and (StateName <> 'Jumpingable') and (StateName <> 'JumpingableMirror') then SetStateNameOne('Down'+mirror,''); 
   end;
   
   procedure Turtle.DownStrike;
   begin
   StrikeSound.Play;
   SetStateNameOne('DownStrike'+mirror,'');
   //while StateName <> 'Down' + mirror do begin end; 
   //Stop; self.SpriteP.Image := self.Sprites[30]; 
   //StateName:='Down'+mirror; //Frame:=2;
   end;
   
   procedure Turtle.HelpJumping(sender: object; e: System.Timers.ElapsedEventArgs);
   begin
   MoveOn(0,-20);
   k-=1;
   if k=0 then tj.Enabled:=False;
   end;
   
   procedure Turtle.Jumping;
   begin 
   MirrorCheck;
   if (StateName <> 'Jumping' + mirror) and (StateName <> 'Jumpingable' + mirror) then 
     begin
      k:=5;
      SetStateNameOne('Jumping'+mirror,'Jumpingable'+mirror);
     end;  
   //if ((GetKeyState(VK_Left) and MASK) > 0) then dv := -15;
   //if ((GetKeyState(VK_Right) and MASK) > 0) then dv := 15; 
   if k=5 then tj.Enabled:=True;
   //for var i:=1 to 20 do
   //MoveOn(round(dv/5),5); 
   end;
   
   procedure Turtle.LiveCheck(l: integer);
   begin
   if ((StateName<>'Jumping') and (StateName<>'Jumpingable') and (StateName<>'JumpingMirror') and (StateName<>'JumpingableMirror')) and (StateName<>'Down') and (StateName<>'DownMirror')
    then
      begin
       SetStateNameOne('Damaging'+mirror, 'Stand'+mirror);
      end;    
   if ind.Live-l>0 then ind.Live-=l else begin Destroy; end;
   //ind.AddindHP(ind.Live,6,Color.Red);
   ind.Repaint;
   end;
   
   procedure Turtle.Teleport;
   begin
   self.Left:=round(GameWindow.Width / 4.2);
   self.Top:=round(GameWindow.Height / 1.454);
   self.StateName := 'Stand';
   end;
   
   destructor Turtle.Destroy;
   begin
   MirrorCheck;
   self.SetStateNameOne('GameOverone'+self.mirror,'GameOvertwo'+self.mirror);
   end;
   
   constructor Foot.Create(x, y: integer);
   begin
   IsGravi:=True;
   self.Left := x;
   self.Top := y;
   Lives := 50;
   self.LoadFromInf('Sprites/Foot/Foot.alepahto');
   TimerII := System.Timers.Timer.Create(random(160,170));
   TimerII.Elapsed += self.ForTimer;
   TimerII.Enabled := False;
   
   TimerLive := System.Timers.Timer.Create(350);
   TimerLive.Elapsed += self.ForTimerTwo;
   TimerLive.Enabled := True;
   
   StrikeSound := Sound.Create('Sounds/Voice/StrikeSword.wav',False);
   end;
   
   procedure Foot.ForTimer(sender: object; e: System.Timers.ElapsedEventArgs);
   var f: boolean:=False;
   begin
   TimerII.Enabled:=f;
   if distant=False then 
              if Shell.Left<self.Left then StateName:='StandMirror' else StateName:='Stand';
   if (distant=False) and (abs(Shell.Left-self.Left)<256) then begin distant:=True; end;
   
   if distant=True then
      Going;
   f:=True;   
   if (Shell.StateName = 'GameOverone') or (Shell.StateName = 'GameOveroneMirror') then begin self.StateName:='Stand'+self.mirror; f:=False; end;   
   TimerII.Enabled:=f;   
   if des=True then TimerII.Enabled:=False;
   end;
   
   procedure Foot.ForTimerTwo(sender: object; e: System.Timers.ElapsedEventArgs);
   begin
   if (Intersect(Shell,self)=True) and (pos('Strike',Shell.StateName)<>0) then self.LiveCheck(15);
   end;
   
   procedure Foot.II(f: boolean);
   begin
   TimerII.Enabled:=f;
   end;
   
   procedure Foot.LiveCheck(l: integer);
   begin
   if livechecks=False then begin
   livechecks:=True;
   if Shell.Left<Left then begin if Left<GameWindow.Width then MoveOn(3,0); end else begin if Left>0 then MoveOn(-3,0); end;
   self.MirrorCheck;
   self.SetStateNameOne('Damaging'+self.mirror,'Stand'+self.mirror);
   Lives-=l;
   if Lives<0 then begin TimerII.Enabled:=False; Destroy; end;
   livechecks:=False;
   end;
   end;
   
   procedure Foot.Going;
   var vect: integer:=0;
   begin
   if InterSect(Shell,self)=False then
   begin
   if Shell.Left<self.Left then begin if self.StateName<>'GoingMirror' then self.StateName:='GoingMirror'; vect:=-5; end;
   if Shell.Left>self.Left then begin if self.StateName<>'Going' then self.StateName:='Going'; vect:=5; end;
   self.Left+=vect;
   end;
   if InterSect(Shell,self)=True then self.Strike;
   end;
   
   procedure Foot.Strike;
   begin
   StrikeSound.Play;
   self.MirrorCheck;
   self.SetStateNameOne('Strike'+self.mirror,'Stand'+mirror);
   sleep(250);
   if (Intersect(Shell,self)) and (pos('Strike',self.StateName)<>0) then begin if Shell.Left>self.Left then Shell.MoveOn(3,0) else Shell.MoveOn(-3,0); Shell.LiveCheck(3); end;
   end;
   
   procedure Foot.MirrorCheck;
   begin
      if self.Left<Shell.Left then self.mirror:='';
      if self.Left>Shell.Left then self.mirror:='Mirror';
   end;
   
   destructor Foot.Destroy;
   begin
   des:=True;
   MirrorCheck;
   self.SetStateNameOne('GameOver'+self.mirror,'');
   sleep(550);
   self.Top:=-256;
   end;
   
   constructor FootY.Create(x, y: integer);
   begin
   IsGravi:=True;
   self.Left := x;
   self.Top := y;
   Lives := 75;
   interval:=19;
   self.LoadFromInf('Sprites/Foot Yellow/Footy.alepahto'); 
   self.sur := GControl.Create();
   self.sur.bm := BitMap.Create('Sprites/Foot Yellow/sur.png');
   self.sur.Left := -100; sur.Top:= -100;
   TimerII := System.Timers.Timer.Create(60);
   TimerII.Elapsed += self.ForTimer;
   TimerII.Enabled := True;
   StrikeSound := Sound.Create('Sounds/Voice/SurikenStrike.wav',False);
   end;
   
   procedure FootY.ForTimer(sender: object; e: System.Timers.ElapsedEventArgs);
   var f: boolean:=False;
   begin
   TimerII.Enabled:=f;
   if Shell.Left<self.Left then self.mirror:='Mirror';
   if Shell.Left>self.Left then self.mirror:='';
   if (self.StateName in StandSet) and (self.StateName<>'Stand'+self.mirror) then self.StateName:='Stand'+self.mirror;
   interval+=1;
   if interval=100 then begin interval:=0; Strike; end;
   sur.Left+=vector;
   if InterSect(Shell,sur) and not(Shell.StateName in DownSet) then begin sur.Left:=-100; sur.Top:=-100; Shell.LiveCheck(2); end;
   if (Intersect(Shell,self)=True) and (pos('Strike',Shell.StateName)<>0) then self.LiveCheck(15);
   f:=True;   
   if (Shell.StateName = 'GameOverone') or (Shell.StateName = 'GameOveroneMirror') then begin self.StateName:='Stand'+self.mirror; f:=False; end;   
   TimerII.Enabled:=f;
   if des=True then TimerII.Enabled:=False;
   end;
   
   procedure FootY.Strike;
   begin
   StrikeSound.Play;
   self.SetStateNameOne('Strike' + self.mirror,'Stand'+self.mirror);
   if mirror='' then vector:=8 else vector:=-8;
   if mirror='' then sur.Left:=self.Left+self.bm.Width else sur.Left:=self.Left; sur.Top:=self.Top+45;
   end;
   
   procedure FootY.LiveCheck(l: integer);
   begin
   if livechecks=False then begin
   livechecks:=True;
   self.MirrorCheck;
   self.SetStateNameOne('Damaging'+self.mirror,'Stand'+self.mirror);
   Lives-=l;
   if Lives<0 then begin TimerII.Enabled:=False; Destroy; end;
   livechecks:=False;
   end;
   end;
   
   destructor FootY.Destroy;
   begin
   des:=True;
   MirrorCheck;
   self.SetStateNameOne('GameOver'+self.mirror,'');
   sleep(550);
   sur.Top:=-256;
   self.Top:=-256;
   end;
   
   constructor Mouser.Create(x, y, a: integer);
   begin
   IsGravi:=True;
   self.Left := x;
   self.Top := y;
   Lives := 60;
   self.LoadFromInf('Sprites/Mouser/Mouser.alepahto');
   self.StateName := 'Going';
   rast:=a;
   proid:=0;
   TimerII := System.Timers.Timer.Create(random(160,170));
   TimerII.Elapsed += ForTimer;
   
   TimerLive := System.Timers.Timer.Create(350);
   TimerLive.Elapsed += self.ForTimerTwo;
   TimerLive.Enabled := True;
   
   //SoundStrike := Sound.Create('',True);
   end;
   
   procedure Mouser.ForTimer(sender: object; e: System.Timers.ElapsedEventArgs);
   var vect: integer;
       fg: boolean:=False;
   begin
   TimerII.Enabled:=fg;
   if f=False then self.mirror:='';
   if f=True then self.mirror:='Mirror';
   if (self.StateName in GoingSet) and (self.StateName<>'Going'+self.mirror) then self.StateName:='Going'+self.mirror;
   if self.mirror='' then vect:=4;
   if self.mirror='Mirror' then vect:=-4;
   if (InterSect(Shell,self)=True) and (self.StateName in GoingSet) then Shell.LiveCheck(2);
   self.Left+=vect;
   proid+=abs(vect);
   if proid>=rast then begin proid:=0; f:=f xor True; end;
   fg:=True;   
   if (Shell.StateName = 'GameOverone') or (Shell.StateName = 'GameOveroneMirror') then begin self.StateName:='Stand'+self.mirror; fg:=False; end;   
   TimerII.Enabled:=fg;
   if des=True then TimerII.Enabled:=False;
   end;
   
   procedure Mouser.ForTimerTwo(sender: object; e: System.Timers.ElapsedEventArgs);
   begin
   if (InterSect(self,Shell)=True) and (pos('Strike',Shell.StateName)<>0) then self.LiveCheck(15);
   end;
   
   procedure Mouser.LiveCheck(l: integer);
   begin
   if livechecks=False then begin
   livechecks:=True;
   self.MirrorCheck;
   self.SetStateNameOne('Damaging'+self.mirror,'Stand'+self.mirror);
   Lives-=l;
   if Lives<0 then begin TimerII.Enabled:=False; Destroy; end;
   livechecks:=False;
   end;
   end;
   
   destructor Mouser.Destroy;
   begin
   des:=True;
   MirrorCheck;
   self.SetStateNameOne('GameOver'+self.mirror,'');
   sleep(550);
   self.Top:=-256;
   end;
   
   //������
   constructor Levels.Create(fname, fmname: string); 
   begin
   bm := Bitmap.Create(fname);
   L:=0;
   self.Left:=0;
   self.Top:=0;
   R:=bm.Width-GameWindow.Width;
   self.BackSound := Sound.Create(fmname,True);
   end;
   
   procedure Levels.Play;
   begin
   BackSound.Play;
   end;
   
   procedure Levels.Stop;
   begin
   BackSound.Stop;
   end;
   
   //���������� 
   constructor Secondmetr.Create();
   begin
   m:=0;
   s:=0;
   milis:=0;
   time := '00:00:00';
   tb := BitMap.Create('Pictures/timer.png');
   bm := Bitmap.Create(200,200);
   g := Graphics.FromImage(bm);
   g.DrawString(time, new System.Drawing.Font('Arial Black', 16, FontStyle.Bold),new SolidBrush(Color.Orange), new PointF(0, 0));
   g.DrawImage(tb,100,0);
   t := System.Timers.Timer.Create(100);
   t.Elapsed += self.ForTimer;
   t.Enabled:=False;
   end;
   
   procedure Secondmetr.ForTimer(sender: object; e: System.Timers.ElapsedEventArgs);
   var a1,a2: string;
   begin
   if m<=9 then a1:='0'+m.tostring else a1:=m.tostring;
   if s<=9 then a2:='0'+s.tostring else a2:=s.tostring;
   time := a1 + ':' + a2 + ':' + (milis/10).ToString;
   g.Clear(Color.Transparent);
   g.DrawString(time, new System.Drawing.Font('Arial Black', 16, FontStyle.Bold),new SolidBrush(Color.Orange), new Point(0, 0));
   g.DrawImage(tb,110,-10);
   milis+=round(t.Interval);
   if milis>=1000 then begin s+=1; milis:=0; end;
   if s>=60 then begin m+=1; s:=0; end;
   end;
   
   procedure Secondmetr.Start;
   begin
   t.Enabled:=True;
   end;
   
   procedure Secondmetr.Clear;
   begin
   m:=0;
   s:=0;
   milis:=0;
   end;
   
   //����������� ��������
   constructor GraphPanel.Create();
   begin
   k:=0; k2:=0;
   GameWindow.Controls.Add(self);
   self.Width := GameWindow.Width;
   self.Height := GameWindow.Height;
   self.BringToFront;
   t := System.Timers.Timer.Create(42);
   t.Elapsed+=self.PaintOne;
   
   t2 := System.Timers.Timer.Create(500);
   t2.Elapsed+=self.PaintThree;
   b := BitMap.Create(self.Width, self.Height);
   g := Graphics.FromImage(b);
   end;
   
   procedure GraphPanel.SetSecondmetr;
   begin
   GSec := Secondmetr.Create();
   GSec.Start;
   end;
   
   procedure GraphPanel.Refreshing; 
   begin
   GTurtle.Parent := GLvl;
   GSec.Clear;
   end;
   
   procedure GraphPanel.AddDop(a: GControl);
   begin
   SetLength(GDop,k2+1);
   a.Parent := GLvl;
   GDop[k2] := a;
   k2+=1;
   end;
   
   procedure GraphPanel.AddGL(a: Levels);
   begin
   GLvl := a;
   t.Enabled:=True;
   end;
   
   procedure GraphPanel.AddGT(a: Turtle);
   begin
   a.LocationChanged += self.PaintTwo;
   a.Parent := GLvl;
   GTurtle := a;
   t.Enabled:=True;
   t2.Enabled:=True;
   end;
   
   procedure GraphPanel.AddGF(a: Character);
   begin
   SetLength(GFoot,k+1);
   a.Parent := GLvl;
   GFoot[k] := a;
   k+=1;
   t.Enabled:=True;
   end;
   
   procedure GraphPanel.Pause;
   begin
   t.Stop;
   t2.Stop;
   GSec.t.Stop;
   for var i:=0 to k-1 do
   GFoot[i].TimerII.Stop;
   IsPausing:=True;
   end;
   
   procedure GraphPanel.Start;
   begin
   t.Start;
   t2.Start;
   GSec.t.Start;
   for var i:=0 to k-1 do
   GFoot[i].TimerII.Start;
   IsPausing:=False;
   end;
   
   procedure GraphPanel.PaintOne(sender:object; e: System.Timers.ElapsedEventArgs);
   var och: boolean:=False;
   begin
   t.Enabled:=False;
   if (pos('Strike',GTurtle.StateName)<>0) then och:=True;
   g.DrawImage(GLvl.bm, new Rectangle(new Point(GLvl.Left, GLvl.Top), new System.Drawing.Size(GLvl.bm.Width, GLvl.bm.Height)));
   if och=False then g.DrawImage(GTurtle.bm, new Rectangle(new Point(GTurtle.Left, GTurtle.Top), new System.Drawing.Size(GTurtle.bm.Width, GTurtle.bm.Height)));
   g.DrawImage(GTurtle.ind.bm, new Rectangle(new Point(GTurtle.ind.Left, GTurtle.ind.Top), new System.Drawing.Size(GTurtle.ind.bm.Width, GTurtle.ind.bm.Height)));
   for var i:=0 to k-1 do
   g.DrawImage(GFoot[i].bm, new Rectangle(new Point(GFoot[i].Left, GFoot[i].Top), new System.Drawing.Size(GFoot[i].bm.Width, GFoot[i].bm.Height)));
   if och=True then g.DrawImage(GTurtle.bm, new Rectangle(new Point(GTurtle.Left, GTurtle.Top), new System.Drawing.Size(GTurtle.bm.Width, GTurtle.bm.Height)));
   if GSec<>nil then g.DrawImage(Gsec.bm,new Rectangle(new Point(450,15),new System.Drawing.Size(GSec.bm.Width, GSec.bm.Height)));
   for var i:=0 to k2-1 do 
   g.DrawImage(GDop[i].bm, new Rectangle(new Point(GDop[i].Left, GDop[i].Top), new System.Drawing.Size(GDop[i].bm.Width, GDop[i].bm.Height)));
   self.Image := b;
   t.Enabled:=True;
   end;
   
   procedure GraphPanel.PaintTwo(sender: object; e: System.EventArgs);
   var vect: integer:=0;
   begin
   if (GTurtle.Left+GTurtle.Width>GameWindow.Width-GameWindow.Width/2.5) and (GLvl.R>0) and (GTurtle.StateName='Going') then vect:=-4;
   if (GTurtle.Left<GameWindow.Width/2.5) and (GLvl.L>0) and (GTurtle.StateName='GoingMirror') then vect:=4;
   GLvl.L -= vect; GLvl.R += vect; 
   GLvl.Left += vect;
   for var i:=0 to k-1 do GFoot[i].Left += vect;
   GLvl.ClientSize := new System.Drawing.Size(GLvl.L,GLvl.R);
   if GTurtle.Left>GLvl.distanend then GLvl.lvlend:=True;
   end;
   
   procedure GraphPanel.PaintThree(sender: object; e: System.Timers.ElapsedEventArgs);
   begin
   t2.Enabled:=False;
   if (GTurtle.IsGravi=True) and (GTurtle.Top<h) and (GTurtle.k=0) then GTurtle.Graviting;
   t2.Enabled:=True;
   end;


Var 
   
   Level: Array of Levels; //������ � ��������
   G: GraphPanel;
   GameMenu: Menu;
   Foots: Array of Foot;
   FootYs: Array of FootY;
   Mousers: Array of Mouser;
   pers: string;
   lvln: integer;
   TG: System.Timers.Timer;

procedure SoundG(sender: object; e: System.EventArgs);
var v: real;
begin
v := GameMenu.SoundBar.Value*0.1;
GameMenu.Music.SetVolume(v);
GameMenu.SoundClick.SetVolume(v);
Shell.StrikeSound.SetVolume(v);
for var i:=0 to 4 do
Foots[i].StrikeSound.SetVolume(v);
for var i:=0 to 1 do
Level[i].BackSound.SetVolume(v);
{if FootYs[0]<>nil then FootYs[0].StrikeSound.SetVolume(v);
if Mousers[0]<>nil then Mousers[0].StrikeSound.SetVolume(v);}
end;

procedure PauseStart;
begin
if G.IsPausing=False then begin G.Pause; GameMenu.PausePanel.Visible:=True; GameMenu.PausePanel.BringToFront; end 
else begin G.Start; GameMenu.PausePanel.Visible:=False; end;
end;

procedure GameContine(sender: object; args: System.EventArgs);
begin
G.Start; GameMenu.PausePanel.Visible:=False;
end;

procedure ScreenSave;
var s: string;
begin
s := System.DateTime.Now.Day.ToString + System.DateTime.Now.Month.ToString + 
System.DateTime.Now.Year.ToString + '_' + System.DateTime.Now.Hour.ToString + 
System.DateTime.Now.Minute.ToString + System.DateTime.Now.Second.ToString;
G.b.Save('Screens\' + s + '.png');
end;

procedure KeyDownThread(param: System.Object);
begin
(*var k := new System.Windows.Forms.KeysConverter();
var k2 := new KeyEventArgs();*)

end;

procedure KeyDown(sender: object; args: KeyEventArgs);
begin
(*var k :=  new System.Windows.Forms.KeysConvertor();
thgs := System.Threading.ParameterizedThreadStart(KeyDownThread);
thg := System.Threading.Thread.Create(thgs);
thg.Start(k.ConvertTo(args,System.Object)); 
*)
case args.KeyCode of
Keys.End: GameWindow.Close;
Keys.O: ScreenSave;
Keys.P: PauseStart;
end;

if G.IsPausing<>True then
begin
case args.KeyCode of
Keys.Left: Shell.GoingL;
Keys.Right: Shell.GoingR;
Keys.Down: Shell.Down;
Keys.Up: Shell.Jumping;
Keys.A: Shell.Strikeone;
Keys.S: Shell.Striketwo;
Keys.D: Shell.Strikethree;
end;
end;
end;

procedure KeyUp(sender: object; args: KeyEventArgs);
begin
case args.KeyCode of
Keys.Left: Shell.Stand;
Keys.Right: Shell.Stand;
Keys.Down: Shell.Stand;
end;
end;

procedure Lvl2;
begin
Level[0].Stop;
Level[1].Play;
G.Pause;
lvln:=2;
for var i:=0 to 4 do
begin
Foots[i].Destroy;
end;
G.AddGL(Level[1]);
G.Refreshing;
Shell.Teleport;

SetLength(FootYs,1);
FootYs[0] := FootY.Create(500, round(GameWindow.Height / 1.454));

SetLength(Mousers,1);
Mousers[0] := Mouser.Create(300,round(GameWindow.Height / 1.454)+48,128);

G.AddGF(Mousers[0]);

G.AddGF(FootYs[0]);
G.AddDop(FootYs[0].sur);
G.Start;
end;

procedure LvlShet(sender: object; e: System.Timers.ElapsedEventArgs);
begin
case lvln of
1: begin if Level[lvln-1].lvlend=True then Lvl2; end;
2: begin end;
end;
end;

procedure Lvl1;
begin
lvln:=1;

Level[0].Play;

TG := System.Timers.Timer.Create(1000);
TG.Elapsed += LvlShet;
TG.Enabled := True;

GameMenu.PreGame;

GameWindow.KeyPreview := True;

G := GraphPanel.Create();

G.SetSecondmetr;

Shell.LoadFromInf('Sprites/' + pers + '/' + pers + '.alepahto');
Shell.Stand;
Shell.AddLives('Pictures\' + pers + 'HP.png');
Shell.LoadSound(pers);

SetLength(Foots,5);
Foots[0] := Foot.Create(50, round(GameWindow.Height / 1.454));
Foots[0].II(True);

Foots[1] := Foot.Create(250, round(GameWindow.Height / 1.454));
Foots[1].II(True);

Foots[2] := Foot.Create(550, round(GameWindow.Height / 1.454));
Foots[2].II(True);

Foots[3] := Foot.Create(650, round(GameWindow.Height / 1.454));
Foots[3].II(True);

Foots[4] := Foot.Create(750, round(GameWindow.Height / 1.454));
Foots[4].II(True);

Level[0].distanend:=530;

G.AddGL(Level[0]);
G.AddGT(Shell);
for var i:=0 to 4 do
G.AddGF(Foots[i]);
end;

procedure GameLeo(sender: object; args: System.EventArgs);
begin
pers := 'Leo';
GameMenu.turtlebut[1].Click -= Gameleo;
Lvl1;
end;

procedure GameDon(sender: object; args: System.EventArgs);
begin
pers := 'Don';
GameMenu.turtlebut[2].Click -= GameDon;
Lvl1;
end;

procedure GameRaph(sender: object; args: System.EventArgs);
begin
pers := 'Raph';
GameMenu.turtlebut[3].Click -= GameRaph;
Lvl1;
end;

procedure GameMike(sender: object; args: System.EventArgs);
begin
pers := 'Mike';
GameMenu.turtlebut[4].Click -= GameMike;
Lvl1;
end;

procedure GameStart(sender: object; args: System.EventArgs);
begin
GameMenu.GameStart;
end;

procedure Options(sender: object; args: System.EventArgs);
begin
GameMenu.Options;
end;

procedure GameCreate(sender: object; args: System.EventArgs);
begin
GameMenu.GameCreate;
end;
   
procedure GameClose(sender: object; args: System.EventArgs);
begin
GameWindow.Close;
end;
   
procedure InitForm;
begin
GameWindow.FormBorderStyle := System.Windows.Forms.FormBorderStyle.FixedSingle;
GameWindow.Height:=480;
GameWindow.Width:=640;
GameWindow.StartPosition:=FormStartPosition.CenterScreen;
GameWindow.Text := 'TMNT Nightmare Dreams';
var Logo := new Icon('IconGame.ico');
GameWindow.Icon:=Logo;
GameWindow.MaximizeBox := False;
end;   

procedure InitMenu(fname: string);
begin
GameWindow.BackgroundImage := System.Drawing.Image.FromFile(fname);

GameMenu := Menu.Create();

GameMenu.btstart.Click += GameStart;
GameMenu.btoptions.Click += Options;
GameMenu.btgamecreate.Click += GameCreate;
GameMenu.btexit.Click += GameClose;

GameMenu.btexittwo.Click += GameClose;
GameMenu.btcontine.Click += GameContine;

GameMenu.turtlebut[1].Click += GameLeo;
GameMenu.turtlebut[2].Click += GameDon;
GameMenu.turtlebut[3].Click += GameRaph;
GameMenu.turtlebut[4].Click += GameMike;

GameMenu.SoundBar.ValueChanged += SoundG;
end;

procedure InitPreviewGame;
begin
SetLength(Level,2);
Level[0] := Levels.Create('Backgrounds/Zone1/Zone1.png', 'Sounds/Music/Lvl1.mp3');
Level[1] := Levels.Create('Backgrounds/Zone2/Zone2.png', 'Sounds/Music/Lvl2.mp3');

Shell := Turtle.Create(round(GameWindow.Width / 4.2), round(GameWindow.Height / 1.454));

GameWindow.KeyDown += KeyDown;
GameWindow.KeyUp += KeyUp;
end;

Begin
InitForm;
InitMenu('Pictures\Menu.jpg');
InitPreviewGame;
Application.Run(GameWindow);
End.   
   